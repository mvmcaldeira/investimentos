package com.investimentos.aplicacao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AplicacaoServiceIT {
  @Autowired
  private AplicacaoService subject;
  
  @Test
  public void deveBuscarUmProduto() {
    Investimento investimento = new Investimento();
    
    Transacao transacao = new Transacao();
    transacao.setValor(1000000000.0);
    
    Cliente cliente = new Cliente();
    cliente.setCpf("123.123.123-12");
    
    Produto produto = new Produto();
    produto.setId(1);
    
    investimento.setCliente(cliente);
    investimento.setProduto(produto);
    investimento.setTransacao(transacao);
    
    Optional<Investimento> optional = subject.criar(investimento);
    Investimento investimentoSalvo = optional.get();
    
    assertNotNull(investimentoSalvo);
    assertNotNull(investimentoSalvo.getTransacao().getTimestamp());
    assertNotNull(investimentoSalvo.getTransacao().getId());
    assertNotNull(investimentoSalvo.getAplicacao().getId());
    assertNotNull(investimentoSalvo.getAplicacao().getDataCriacao());
  }
}
